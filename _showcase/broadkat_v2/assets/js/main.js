var nbContentBlock = 0,
    current = 0,
    target = 0,
    scrollUpQueue = 0,
    scrollDownQueue = 0,
    scrollQueueMax = 1,
    scrollDuration = 500; //3;
var moving = false;

var navigate = function(target_, mousesrc)
{
    if(!moving)
    {
        target = target_;
        moving = true;
        
        $(".navigationLink").removeClass("activeMenu");
        $(".navigationLink:eq("+target+")").addClass("activeMenu");

        $('.contentBlock').removeClass("blockVisible");
        $('.contentBlock:eq(' + target + ')').addClass("blockShowing");
        $("html").scrollTo($('.contentBlock:eq(' + target + ')'), scrollDuration, {
            onAfter: function () {
                $('.contentBlock:eq(' + target + ')').removeClass("blockShowing");
                $('.contentBlock:eq(' + target + ')').addClass("blockVisible");

//                console.log( "mousesrc = ", mousesrc );
                if(mousesrc) setTimeout(function(){ moving = false; }, scrollDuration * 1.5);
                else                                moving = false;
                
                current = target;
                scrollUpQueue = 0;
                scrollDownQueue = 0;
            }
        });
    }
};

$(document).ready(function () {
		
    /* MAKE SURE WE START FROM FIRST PAGE */
    current = 0;
    // window.location = "#work";
    nbContentBlock = $(".contentBlock").length - 1;
    
    $('#burger').click(function(){
		$(this).toggleClass('open');
        $("#submenu").toggleClass('visibleSub');
	});
	    
    /* PORTFOLIO TILE SIZE WORK-AROUND (firefox issue) */
    $(window).on("resize", function () {
        if ($("body").hasClass("mobile"))
            $("#portfolio .projectTileContainer").css( "height", ($(this).height() - 50) / 6 + "px" );
        else
            $("#portfolio .projectTileContainer").css( "height", ($(this).height() - 50) / 2 + "px" );
    }).resize();


    // LINK NAVIGATION
    $(".navigationLink,#calliLink").click(function () {
        var target_ = $(this).attr("rel");
        moving = false; // this allows faster link browsing
        navigate(target_, false);
    });
    
    // KEY NAVIGATION
     $( document ).keydown(function(event) {
         var down = 40, up = 38, left = 37, right = 39;
         if (event.which==up || event.which==down) {
             var target_ = current;
             if(event.which==up)    target_ --;
             else				    target_ ++;
             if(target_>nbContentBlock) target_ = 0;
			 if(target_<0) target_ = 0;
            navigate(target_, false);
         }
     });

	 
    // SCROLL NAVIGATION
    $(".contentBlock").each(function () {
        $(this).mousewheel(function (event, delta) {
//            console.log("mouse wheeled !", event, delta);
            event.preventDefault();            
            var target_ = current;
            if (event.deltaY > 0) {
                target_--;
                if(target_<0) target_ = 0;
                navigate(target_, true);
            }
            else {
                target_++;
                if (target_ > nbContentBlock) target_ = 0;
                navigate(target_, true);
            }
        });
    });
       
    
    // TILES ACTION
    $(".tile").addClass("inactiveTile");
    $(".tile").click(function () {
        if ($(this).hasClass("tileSelected")) {
            $(this).find(".tileContentBlock").removeClass("activeTileBlock");            
            var tmp = $(this);
            setTimeout(function(){ $(".tile").addClass("inactiveTile"); tmp.removeClass("tileSelected"); }, 1000);
        } else {
            $(".tile").removeClass("inactiveTile");
            $(this).addClass("tileSelected");
            var tmp = $(this);
            setTimeout(function(){ tmp.find(".tileContentBlock").addClass("activeTileBlock"); }, 1000);
        }
    });

    // DOC READY ANIMATIONS
    $("body").addClass("bodyLoaded");

    
	// SWIPE NAVIGATION
	$("body.mobile").swipe( {
		//Generic swipe handler for all directions
		swipe:function(event, direction, distance, duration, fingerCount, fingerData) 
		{
			var target_ = current;
			if ( direction == 'up' )		target_ ++;
			else if ( direction == 'down' )	target_ --;
            if(target_>nbContentBlock) target_ = 0;
			if(target_<0) target_ = 0;
			navigate(target_, true);
		}
	});


    // Works : switch between clients
    $("#details-subnav a").click(function()
     {
        // change the visible details container
        var target = $(this).attr('details-target');
        $('.details-container')     .removeClass('visibleDetails');
        $("#" + target + "-details").addClass('visibleDetails');
        
        $('.details-video') .removeClass('visibleDetails');
        $("#" + target + "-video")         .addClass('visibleDetails');
        
        // change the selected client menu item
        $('#details-subnav a')  .removeClass('subnavSelected');
        $(this)                 .addClass('subnavSelected');
        
    });
    
});
