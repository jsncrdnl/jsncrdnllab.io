<?php

function render_view($view, $data = NULL)
{
    if ($data) extract($data);
    require('templates/renders/' . $view . '.php');
}
