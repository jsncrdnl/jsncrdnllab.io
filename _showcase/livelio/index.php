<?php
require 'controllers.php';
require 'functions.php';
?>
<!doctype html>
<html lang="en">

<head>
	<title>Livelio</title>
	<?php include 'templates/parts/head.php'; ?>
	<?php include 'templates/parts/css.php'; ?>
</head>

<body>

	<?php include 'templates/parts/menu.php'; ?>

	<div id="header" class="container-fluid position-relative">

		<?php include 'templates/parts/header-text.php'; ?>

	</div>

	<?php include "templates/$view.php"; ?>
	<?php include "templates/parts/footer.php"; ?>
	<?php include 'templates/parts/js.php'; ?>

</body>

</html>