<!-- <div class="fluid-image w-100 position-absolute header-image" 
    src="assets/img/images/900px/Header/LIVELIO-header_Home-900px.jpg"></div> -->

<img class="fluid-image w-100 position-absolute" src="assets/img/images/900px/Header/LIVELIO-header_Home-900px.jpg" />
<img class="fluid-image w-100 position-absolute" src="assets/img/images/1300px/Header/LIVELIO-header_Home-1300px.jpg" />
<img class="fluid-image w-100 position-absolute" src="assets/img/images/1920px/Header/LIVELIO-header_Home-1920px.jpg" />
<img class="fluid-image w-100 position-absolute" src="assets/img/images/2560px/Header/LIVELIO-header_Home-2560px.jpg" />
<!-- 
<div id="header-image-container">
    <div id="header-image"></div>
</div> -->


<nav class="navbar navbar-expand-lg navbar-light d-block" style="height: 140px">

    <div class="d-flex flex-row float-left mt-1">
        <div class="d-none d-sm-block">
            <a class="text-decoration-none" href="#" target="_blank">
                <img src="assets/img/icons/SVG/LIVELIO_logo_fin_fondtransparent_RVB.svg" style="height: 100px" />
            </a>
        </div>
    </div>

    <a class="navbar-brand" href="#">&nbsp;</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse float-right pt-5 pr-5 text-black" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item mx-3 active">
                <a class="nav-link font-weight-bold" href="?view=home">Home</a>
            </li>
            <li class="nav-item mx-3">
                <a class="nav-link font-weight-bold" href="?view=vision-strategy">Vision et stratégie</a>
            </li>
            <li class="nav-item mx-3">
                <a class="nav-link font-weight-bold" href="?view=group">Le groupe</a>
            </li>
            <li class="nav-item mx-3">
                <a class="nav-link font-weight-bold" href="?view=men">Les Hommes</a>
            </li>
            <li class="nav-item mx-3">
                <a class="nav-link font-weight-bold" href="?view=news">Actualités</a>
            </li>
            <li class="nav-item mx-3">
                <a class="nav-link font-weight-bold" href="?view=contact">Contact</a>
            </li>
        </ul>
    </div>
</nav>