<div class="row vw-100 text-left text-white pt-5" id="footer-image">
    <div class="row mx-auto mt-5 pt-5 mb-5 pb-5">

        <div class="col-3 pt-5">
            <span class="font-italic font-weight-light pb-5 d-block">CONNECT</span>
            <a class="text-decoration-none d-block font-weight-bold text-white" href="#">
                LinkedIn
            </a>
            <a class="text-decoration-none d-block font-weight-bold text-white" href="#">
                YouTube
            </a>
            <a class="text-decoration-none d-block font-weight-bold text-white" href="#">
                Facebook
            </a>
        </div>
        <div class="col-3 pt-5">
            <span class="font-italic font-weight-light  pb-5 d-block">ABOUT US</span>
            <a class="text-decoration-none d-block font-weight-bold text-white" href="#">
                Politique de<br /> confidentialité
            </a>
            <a class="text-decoration-none d-block font-weight-bold text-white" href="#">
                FAQ
            </a>
        </div>

        <div class="col-4">

        </div>
    </div>
</div>