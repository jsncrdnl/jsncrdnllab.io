<div class="row text-green ml-5 mt-5 pt-5 pl-5 pb-3">
    <h1 class="font-bauer font-weight-bold header-01">
        Livelio
    </h1>
</div>
<div class="row text-white ml-5 pl-5 pb-3 font-weight-bold header-02">
    Biologie et construction<br />
    de projets
</div>
<div class="row ml-5 pl-5">
    <h3 class="d-inline-block text-white font-weight-light font-italic p-1 header-03">
        Proximité et excellence
    </h3>
</div>


<div class="position-absolute d-flex flex-row verticallinks">
    <a class="text-decoration-none mx-3" href="https://linkedin.com" target="_blank">
        LinkedIn
    </a>
    <a class="text-decoration-none mx-3" href="https://youtube.com" target="_blank">
        YouTube
    </a>
    <a class="text-decoration-none mx-3" href="https://facebook.com" target="_blank">
        Facebook
    </a>
</div>