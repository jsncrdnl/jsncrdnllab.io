    <div id="content-first-block" class="row row-max-width mx-auto pb-2 mb-2 text-center" style="padding-top: 30px;">
      <div class="col-lg-6 col-12 mt-5 pt-5 text-left">
        <p class="text-left pt-2 font-weight-bold" data-aos="fade-up">
          Luxembourg Chamber of Commerce - International Affairs<br />
          Enterprise Europe Network - Luxembourg
        </p>
        <p class="" data-aos="fade-up">
          7, rue Alcide de Gasperi<br />
          L-2981 Luxembourg
        </p>
        <p data-aos="fade-up">
          Ms Amrita Singh, International Affairs Advisor, EEN Project Manager<br />
          Mr Luca Mancuso, Junior Project Officer
        </p>
        <p data-aos="fade-up">
          <span class="font-weight-bold">Tel: </span>+352 42 39 39 377 / 532<br />
          <span class="font-weight-bold">E-mail: </span>EMAIL TBC<br />
          <span class="font-weight-bold">Phone: </span>+352 42 39 39 370<br />
          <span class="font-weight-bold">Fax: </span>+352 43 83 26
        </p>
      </div>
      <div class="col-lg-6 col-12 mt-5 side-img" style="min-height: 550px;" data-aos="fade-up" data-aos-duration="1500">
        <img class="floating floating-delay-1" src="assets/img/2107_B2FAIR_Page-contact2.png">
        <img class="floating" src="assets/img/2107_B2FAIR_Page-contact1.png">
        <img class="floating floating-delay-2" src="assets/img/2107_B2FAIR_Page-contact3.png">
      </div>
    </div>

    <div id="footer-image" class="row pt-5 mt-5">

      <div id="footer-gallery" class="w-100 d-flex flex-row justify-content-center align-bottom">
        <div class="px-4 py-5">
          <a class="text-decoration-none" href="https://www.b2fair.com/" target="_blank">
            <img src="assets/img/b2fair-logo-cc.svg" style="height: 50px;" class="mt-3" />
          </a>
        </div>
        <div class="px-4 py-5">
          <a class="text-decoration-none" href="https://www.luxembourgexpo2020dubai.lu/en/" target="_blank">
            <img src="assets/img/b2fair-logo-pavillon.svg" style="height: 65px;" />
          </a>
        </div>
        <div class="px-4 py-5 my-2">
          <a class="text-decoration-none" href="https://www.tradeandinvest.lu" target="_blank">
            <img src="assets/img/b2fair-logo-tradeinvest.svg" style="height: 25px;" class="mt-3" />
          </a>
        </div>
      </div>

      <div id="footer" class="w-100 m-0 p-0" style="font-size: 10px;">
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="index.html">
          HOME
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="participation.html">
          PARTICIPATION, CONDITIONS & PROCEDURES
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="programme.html">
          PROGRAMME
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="partners.html">
          PARTNERS
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="participants.html">
          PARTICIPANTS
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="access.html" target=_blank>
          ACCESS
          THE PLATFORM</a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="contact.html">
          CONTACT
        </a>
        <div class="py-1 pl-5 d-block d-lg-inline-block text-center float-right pr-2">
          &copy; Luxembourg Chamber of Commerce - All rights reserved 2021
        </div>
        <div class="clearfix" />
      </div>

    </div>