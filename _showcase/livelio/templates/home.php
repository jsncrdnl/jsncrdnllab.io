<div class="container">
    <div class="row row-max-width mx-auto pb-2 mb-2 text-left">

        <div class="col-lg-4 offset-lg-2 col-12 side-img d-table" data-aos="fade-up" data-aos-duration="1500">
            <div class="align-middle d-table-cell">
                <h3 class="text-green p-1 mx-auto text-uppercase font-weight-light h6" data-aos="fade-up">
                    Bienvenue sur livelio
                </h3>
                <h3 class="d-inline-block text-black p-1 mx-auto font-weight-bold" data-aos="fade-up">
                    bol bolobolo
                </h3>
                <p><a class="custom-link small" href="#">En savoir plus</a></p>
            </div>
        </div>

        <div class="col-lg-6 col-12 text-left border-green border-top-0 border-bottom-0 border-right-0">

            <div class="row row-max-width mx-auto pb-1 text-center">
                <p class="" data-aos="fade-up">
                    Une stratégie créatrice de pôles d’excellence pour offrir au patient un parcours d’accompagnement complet et aux praticiens un environnement dynamique en perpétuelle innovation
                </p>
            </div>

            <div class="row row-max-width mx-auto text-center pl-3">
                <div class="col-4">
                    <img class="mx-auto m-3" src="assets/img/icons/SVG/Pictogrammes/LIVELIO-picto_hosto.svg" data-aos="fade-zoom-in" data-aos-duration="1000" style="height: 100px;"><br />
                    <p class="text-green font-weight-bold" data-aos="fade-up">XXX</p>
                    Cliniques
                </div>
                <div class="col-4">
                    <img class="mx-auto m-3" src="assets/img/icons/SVG/Pictogrammes/LIVELIO-picto_dossier.svg" data-aos="fade-zoom-in" data-aos-duration="1000" style="height: 100px;"><br />
                    <p class="text-green font-weight-bold" data-aos="fade-up">XXX</p>
                    spécialités
                </div>
                <div class="col-4">
                    <img class="mx-auto m-3" src="assets/img/icons/SVG/Pictogrammes/LIVELIO-picto_microscope.svg" data-aos="fade-zoom-in" data-aos-duration="1000" style="height: 100px;"><br />
                    <p class="text-green font-weight-bold" data-aos="fade-up">XXX</p>
                    laboratoires
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row vw-100 py-5 my-5 text-center">
    <div class="col-4">
        <img class="w-100" src="assets/img/images/1920px/Secondaires/Livelio_imagesecondaire1-HOME-1920px.jpg" data-aos="fade-zoom-in" data-aos-duration="1000"><br />
    </div>
    <div class="col-4">
        <img class="w-100" src="assets/img/images/1920px/Secondaires/Livelio_imagesecondaire3-HOME-1920px.jpg" data-aos="fade-zoom-in" data-aos-duration="1000"><br />
    </div>
    <div class="col-4">
        <img class="w-100" src="assets/img/images/1920px/Secondaires/Livelio_imagesecondaire2-HOME-1920px.jpg" data-aos="fade-zoom-in" data-aos-duration="1000"><br />
    </div>
</div>

<!-- <h1 class="tlt" data-in-effect="fadeInUp">Title</h1> -->


<!-- <h1 class="ml13">Rising Strong</h1> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
<script>
    // $(function() {
    //     $('.tlt').textillate();
    // })
    // Wrap every letter in a span
    var textWrapper = document.querySelector('.ml13');
    textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

    anime.timeline({
            loop: false,
            autoplay: false
        })
        .add({
            targets: '.ml13 .letter',
            translateY: [100, 0],
            translateZ: 0,
            opacity: [0, 1],
            easing: "easeOutExpo",
            duration: 1400,
            delay: (el, i) => 300 + 30 * i
        });
</script>
<style>
    /* .ml13 { */
    /* font-size: 1.9em; */
    /* letter-spacing: 0.5em; */
    /* font-weight: 600; */
    /* text-transform: uppercase; */
    /* } */

    .ml13 .letter {
        display: inline-block;
        line-height: 1em;
    }
</style>


<div class="row my-3 pb-3" data-aos="fade-up" data-aos-duration="1500">
    <div class="col-6 offset-3 text-center">
        <h3 class="text-green p-1 mx-auto text-uppercase font-weight-light h6" data-aos="fade-up">
            L'équipe
        </h3>
        <h3 class="d-inline-block text-black p-1 mx-auto font-weight-bold" data-aos="fade-up">
            Une équipe de professionnels<br />
            au service d'un développement constant
        </h3>
        <p><a class="custom-link small" href="#">En savoir plus</a></p>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 offset-lg-3 col-12">

        <div class="row text-left pt-5">
            <div class="col-2">
                <div style="height: 130px; background: url(//via.placeholder.com/200) no-repeat; background-size: cover; background-position: center center;" data-aos="fade-up" data-aos-duration="1500"></div>
                <div style="width: 130px; height: 130px;">
                    <p class="font-weight-bold pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">JLO</p>
                    <p class="pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">bla bla bla</p>
                </div>
                <div style="height: 130px; background: url(//via.placeholder.com/200) no-repeat; background-size: cover; background-position: center center;" data-aos="fade-up" data-aos-duration="1500"></div>
                <div style="width: 130px; height: 130px;">
                    <p class="font-weight-bold pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">JLO</p>
                    <p class="pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">bla bla bla</p>
                </div>
            </div>
            <div class="col-2">
                <div style="width: 130px; height: 130px;"></div>
                <div style="height: 130px; background: url(//via.placeholder.com/200) no-repeat; background-size: cover; background-position: center center;" data-aos="fade-up" data-aos-duration="1500"></div>
                <div style="width: 130px; height: 130px;">
                    <p class="font-weight-bold pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">JLO</p>
                    <p class="pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">bla bla bla</p>
                </div>
                <div style="width: 130px; height: 130px;"></div>
            </div>
            <div class="col-2">
                <div style="height: 130px; background: url(//via.placeholder.com/200) no-repeat; background-size: cover; background-position: center center;" data-aos="fade-up" data-aos-duration="1500"></div>
                <div style="width: 130px; height: 130px;">
                    <p class="font-weight-bold pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">JLO</p>
                    <p class="pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">bla bla bla</p>
                </div>
                <div style="height: 130px; background: url(//via.placeholder.com/200) no-repeat; background-size: cover; background-position: center center;" data-aos="fade-up" data-aos-duration="1500"></div>
                <div style="width: 130px; height: 130px;">
                    <p class="font-weight-bold pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">JLO</p>
                    <p class="pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">bla bla bla</p>
                </div>
            </div>
            <div class="col-2">
                <div style="width: 130px; height: 130px;"></div>
                <div style="height: 130px; background: url(//via.placeholder.com/200) no-repeat; background-size: cover; background-position: center center;" data-aos="fade-up" data-aos-duration="1500"></div>
                <div style="width: 130px; height: 130px;">
                    <p class="font-weight-bold pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">JLO</p>
                    <p class="pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">bla bla bla</p>
                </div>
                <div style="width: 130px; height: 130px;"></div>
            </div>
            <div class="col-2">
                <div style="height: 130px; background: url(//via.placeholder.com/200) no-repeat; background-size: cover; background-position: center center;" data-aos="fade-up" data-aos-duration="1500"></div>
                <div style="width: 130px; height: 130px;">
                    <p class="font-weight-bold pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">JLO</p>
                    <p class="pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">bla bla bla</p>
                </div>
                <div style="height: 130px; background: url(//via.placeholder.com/200) no-repeat; background-size: cover; background-position: center center;" data-aos="fade-up" data-aos-duration="1500"></div>
                <div style="width: 130px; height: 130px;">
                    <p class="font-weight-bold pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">JLO</p>
                    <p class="pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">bla bla bla</p>
                </div>
            </div>
            <div class="col-2">
                <div style="width: 130px; height: 130px;"></div>
                <div style="height: 130px; background: url(//via.placeholder.com/200) no-repeat; background-size: cover; background-position: center center;" data-aos="fade-up" data-aos-duration="1500"></div>
                <div style="width: 130px; height: 130px;">
                    <p class="font-weight-bold pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">JLO</p>
                    <p class="pt-3" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">bla bla bla</p>
                </div>
                <div style="width: 130px; height: 130px;"></div>
            </div>
        </div>
    </div>
</div>


<div class="row vw-100 py-3 my-5 text-center bg-green">
    <div class="row mx-auto">

        <div class="col-4">
            <img class="w-100" src="//via.placeholder.com/500" data-aos="fade-zoom-in" data-aos-duration="1000"><br />
        </div>
        <div class="col-4 d-table">
            <div class="d-table-cell align-middle">


                <h3 class="text-white p-1 mx-auto text-uppercase font-weight-light h6" data-aos="fade-up">
                    Rencontres
                </h3>
                <h3 class="d-inline-block text-black p-1 mx-auto font-weight-bold" data-aos="fade-up">
                    Gourvenance
                </h3>
                <p><a class="custom-white-link small" href="#">Plus d'interviews</a></p>

            </div>
        </div>

        <div class="col-4">
            <img class="w-100" src="//via.placeholder.com/500" data-aos="fade-zoom-in" data-aos-duration="1000"><br />
        </div>
    </div>
</div>

<div class="container">

    <div class="row my-3 pb-3" data-aos="fade-up" data-aos-duration="1500">
        <div class="col-12 text-left">
            <h3 class="text-green p-1 mx-auto text-uppercase font-weight-light h6" data-aos="fade-up">
                Groupe et établissements
            </h3>
            <h3 class="d-inline-block text-black p-1 mx-auto font-weight-bold" data-aos="fade-up">
                Actualités
            </h3>
            <p><a class="custom-link small" href="#">Plus d'actualités</a></p>
        </div>
    </div>


    <div class="row">
        <div class="col-12 mb-5 pb-5">

            <div class="row text-center pt-5">
                <?php render_view('news', [
                    'img' => 'assets/img/images/1920px/Secondaires/Livelio_imagesecondaire1-VISION-1920px.jpg',
                    'day' => '19',
                    'month' => 'Février',
                    'name' => 'bla bla bla',
                ]); ?>
                <?php render_view('news', [
                    'img' => 'assets/img/images/1920px/Secondaires/Livelio_imagesecondaire2-VISION-1920px.jpg',
                    'day' => '20',
                    'month' => 'Mars',
                    'name' => 'ihireipghp ergokerogker bla',
                ]); ?>
                <?php render_view('news', [
                    'img' => 'assets/img/images/1920px/Secondaires/Livelio_imagesecondaire2-HOME-1920px.jpg',
                    'day' => '21',
                    'month' => 'Avril',
                    'name' => 'bla bl xxxxa bla',
                ]); ?>
            </div>

            <div class="row text-center pt-5">
                <?php render_view('news', [
                    'img' => 'assets/img/images/1920px/Secondaires/Livelio_imagesecondaire1-VISION-1920px.jpg',
                    'day' => '22',
                    'month' => 'Juin',
                    'name' => 'plplao jnroughverouver',
                ]); ?>
                <?php render_view('news', [
                    'img' => 'assets/img/images/1920px/Secondaires/Livelio_imagesecondaire2-VISION-1920px.jpg',
                    'day' => '23',
                    'month' => 'Juillet',
                    'name' => 'oiuytre nnhgfds bla',
                ]); ?>
                <?php render_view('news', [
                    'img' => 'assets/img/images/1920px/Secondaires/Livelio_imagesecondaire2-HOME-1920px.jpg',
                    'day' => '24',
                    'month' => 'Aout',
                    'name' => 'plplf fgrppfiorejfpe fffefief',
                ]); ?>
            </div>
        </div>
    </div>

</div>