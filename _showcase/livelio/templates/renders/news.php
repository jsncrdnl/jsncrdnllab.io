<div class="col-4">
    <div data-aos="fade-up" data-aos-duration="1500">
        <div style="background: url(<?= $img ?>) no-repeat; background-size: cover; background-position: center center; height: 200px" class="w-100"></div>
        <div style="height: 5px; background: rgb(118,198,201); background: linear-gradient(90deg, rgba(118,198,201,1) 3%, rgba(1,127,173,1) 100%);" class="w-100"></div>
    </div>
    <div class="p-2">
        <div class="text-red pr-3 h2 font-weight-bold news-date float-left" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500"><?= $day ?></div>
        <div class="font-weight-bold text-left" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500">
            <?= $month ?> <?= $year ?>
        </div>
        <div class="text-left" data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="500" style="margin-top: -5px">
            <?= $name ?>
        </div>
    </div>
</div>