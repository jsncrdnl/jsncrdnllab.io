var nbContentBlock = 0,
    current = 0,
    target = 0,
    scrollUpQueue = 0,
    scrollDownQueue = 0,
    scrollQueueMax = 1,
    scrollDuration = 500; //3;
var moving = false;
var sendingMail = false;

var navigate = function(target_, mousesrc)
{
    if(!moving) 
    {        
        $("#modal").removeClass("visible");
        
        target = target_;
        moving = true;
//        console.log("target is " + target);
        $(".navigationLink").removeClass("activeMenu");
        $(".navigationLink:eq("+target+")").addClass("activeMenu");

        $('.contentBlock').removeClass("blockVisible"); 
        $('.contentBlock:eq(' + target + ')').addClass("blockShowing");
        console.log("target = ", target);
        
        var targetID = $('.contentBlock:eq(' + target + ')').attr('id');
        $('html').animate({
            scrollTop: $("#"+targetID).offset().top
        }, scrollDuration, "swing", function() {
            
            $('.contentBlock:eq(' + target + ')').removeClass("blockShowing");
            $('.contentBlock:eq(' + target + ')').addClass("blockVisible");

            if(mousesrc) setTimeout(function(){ moving = false; }, scrollDuration * 1.5);
            else                                moving = false;
            
            current = target;
            scrollUpQueue = 0;
            scrollDownQueue = 0;
        });
        
//        $("html").scrollTo($('.contentBlock:eq(' + target + ')'), scrollDuration, {
//            onAfter: function () {
//                $('.contentBlock:eq(' + target + ')').removeClass("blockShowing");
//                $('.contentBlock:eq(' + target + ')').addClass("blockVisible");
//
//                if(mousesrc) setTimeout(function(){ moving = false; }, scrollDuration * 1.5);
//                else                                moving = false;
//                
//                current = target;
//                scrollUpQueue = 0;
//                scrollDownQueue = 0;
//            }
//        });
    }
};

$(document).ready(function () {
	
	/* MAKE SURE MODAL IS HIDDEN */
	$("#modal").removeClass("visible");
	
    /* MAKE SURE WE START FROM FIRST PAGE */
    current = 0;
    // window.location = "#work";
    nbContentBlock = $(".contentBlock").length - 1;

    /* STICK THE MENU */
    $("nav").sticky({
        topSpacing: 0
    });
	
	/* PORTFOLIO */
	$("#portfolio .projectTileContainer").click(function(){
//		$("#modalImg").css('backgroundImage', $(this).find(".GrayBackground").css("backgroundImage") );
//        console.log('' + $(this).find(".GrayBackground").css("backgroundImage") + ' no-repeat, white');
		$("#modalImg").css('background', '' + $(this).find(".GrayBackground").css("backgroundImage") + ' no-repeat, white' );
		$("#modalImg").css('background-position-x', '50%' );
		$("#modalImg").css('background-size', 'contain' );
		$("#modalTitle").text( $(this).find(".projectTitle").text() );
		$("#modalDesc").html( $(this).find(".projectDesc").html() );
		$("#modal").addClass("visible");
	});
	$("#modal").click(function(){
		$("#modal").removeClass("visible");
	});
    
    /* PORTFOLIO TILE SIZE WORK-AROUND (firefox issue) */
    $(window).on("resize", function () {
        if ($("body").hasClass("mobile"))
            $("#portfolio .projectTileContainer").css( "height", ($(this).height() - 50) / 6 + "px" );
        else
            $("#portfolio .projectTileContainer").css( "height", ($(this).height() - 50) / 2 + "px" );
    }).resize();

    /* CONTACT FORM */
    $(".customInput").click(function () {
        // $(".customInput .inputDecoration").removeClass("active");
        $(".customInput .inputField").each(function (index, value) {
            var val = $(this).val();
            if (val == null || val == "")
                $(this).parent().find(".inputDecoration").removeClass("active");
        });

        $(this).find(".inputDecoration").addClass("active");
        $(this).find(".inputField").focus();
    });
    $(".inputField").focusout(function () {}, function () {
        var val = $(this).val();
        if (val == null || val == "")
            $(this).parent().find(".inputDecoration").removeClass("active");
        // $(".customInput .inputDecoration").removeClass("active");
    });
    $(".inputField").focus(function () {
        $(this).parent().find(".inputDecoration").addClass("active");
    });

//    $("#planeBtn img").click(function () {
//        $(this).stop().velocity({
//            'translateX': '2000px',
//            'translateY': '-2000px)'
//        }, 1000, [.32, -0.43, .8, 1.48], function () {
//            $(this).stop().velocity({
//                'translateX': '-2000px',
//                'translateY': '2000px)'
//            }, 0);
//            $(this).stop().velocity({
//                'translateX': '0px',
//                'translateY': '0px)'
//            }, 500, [.37, .78, .25, .81]);
//        });
//    });

    // HOME SCREEN CLICK NAVIGATION
    $("#preload, #home").click(function () {
        navigate(1, false);
    });

    // LINK NAVIGATION
    $(".navigationLink,#calliLink").click(function () {
        var target_ = $(this).attr("rel");
        moving = false; // this allows faster link browsing
        navigate(target_, false);
    });
    
    // KEY NAVIGATION
     $( document ).keydown(function(event) {
         var down = 40, up = 38, left = 37, right = 39;
         if (event.which==up || event.which==down) {
             var target_ = current;
             if(event.which==up)    target_ --;
             else				    target_ ++;
             if(target_>nbContentBlock) target_ = 0;
			 if(target_<0) target_ = 0;
            navigate(target_, false);
         }
     });

	 
    // SCROLL NAVIGATION
    $(".contentBlock").each(function () {
        $(this).mousewheel(function (event, delta) {
            event.preventDefault();            
            var target_ = current;
            if (event.deltaY > 0) {
                target_--;
                if(target_<0) target_ = 0;
                navigate(target_, true);
            }
            else {
                target_++;
                if (target_ > nbContentBlock) target_ = 0;
                navigate(target_, true);
            }
        });
    });
       
    
    // TILES ACTION
    $(".tile").addClass("inactiveTile");
    $(".tile").click(function () {
        if ($(this).hasClass("tileSelected")) {
            $(this).find(".tileContentBlock").removeClass("activeTileBlock");            
            var tmp = $(this);
            setTimeout(function(){ $(".tile").addClass("inactiveTile"); tmp.removeClass("tileSelected"); }, 1000);
        } else {
            $(".tile").removeClass("inactiveTile");
            $(this).addClass("tileSelected");
            var tmp = $(this);
            setTimeout(function(){ tmp.find(".tileContentBlock").addClass("activeTileBlock"); }, 1000);
        }
    });

    // DOC READY ANIMATIONS
    $("body").addClass("bodyLoaded");

    if ($("body").hasClass("mobile")) 
    {         
        // mobile specific     
//        $(".responsiveText").fitText(2.0);        
        $(".tileContentBlockWrapper").owlCarousel({
			margin:10,
    // autoWidth:true,
    // items:4
            // autoWidth:true 
        });
    }
    else
    {
        // desktop specific
        $("body").addClass("desktop");
        		
//		// THIS SHOULD BE THE INIT OF THE TRIANGULATION
//		//$("div#whoarewe div.subContainer").text();

    } 
    
    
	// SWIPE NAVIGATION
	$("body.mobile").swipe( {
		//Generic swipe handler for all directions
		swipe:function(event, direction, distance, duration, fingerCount, fingerData) 
		{
			var target_ = current;
			if ( direction == 'up' )		target_ ++;
			else if ( direction == 'down' )	target_ --;
            if(target_>nbContentBlock) target_ = 0;
			if(target_<0) target_ = 0;
			navigate(target_, true);
		}
	});
 
	// Add team members click action on desktop
    $("body.desktop .teamMember .teamPicture").click(function()
	{
		var hasClass_ = $(this).parent().hasClass("selectedTM");
		$(".teamMember").removeClass("selectedTM");		
		if(!hasClass_) $(this).parent().addClass("selectedTM");
	});

     $( "#contact input" ).keydown(function(event) {
         if (event.which==13)
            $("#contact #formSubmit").click();
     });

    $("#contact #formSubmit").click(function()
    {
        var allGood = true;

        if ( !validateSimpleText($("#formName input").val()) ){
            $("#formName .errorContainer .error").addClass("errorActive");
            allGood = false;
        }
        else    $("#formName .errorContainer .error").removeClass("errorActive");

        if ( !validateSimpleText($("#formSubject input").val()) ){
            $("#formSubject .errorContainer .error").addClass("errorActive");
            allGood = false;
        }
        else    $("#formSubject .errorContainer .error").removeClass("errorActive");

        if ( !validateEmail($("#formMail input").val()) ){
            $("#formMail .errorContainer .error").addClass("errorActive");
            allGood = false;
        }
        else    $("#formMail .errorContainer .error").removeClass("errorActive");

        if ( $("#formMessage textarea").val().length<10 ){
            $("#formMessage .errorContainer .error").addClass("errorActive");
            allGood = false;
        }
        else    $("#formMessage .errorContainer .error").removeClass("errorActive");

        if(allGood)
        {
            if(!sendingMail)
            {
                sendingMail = true;
                $.ajax({
                    url: "sendMail.php",
                    method: "POST",
                    data:{
                        user: $("#formName input").val(),
                        subj: $("#formSubject input").val(),
                        mail: $("#formMail input").val(),
                        msg: $("#formMessage textarea").val()
                    }
                }).success(function(data){
                    if(data==1)
                        alert("Message envoyé avec succès !");
                    else
                        alert("Échec de l'envoi du message. Cause 01");
                })
                .fail(function(e){
                    alert("Échec de l'envoi du message. Cause 02");
                })
                .always(function(){
                    sendingMail = false;
                });
            }
            else
                alert("Message en cours de traitement ...");
        }
    });


    /*setTimeout(function () {
        $("#preload").css("display", "none");
    }, 1000);*/

});


function validateSimpleText(txt) {
    var re = /^[a-zA-ZéàèàëïöäüâêûîôçÁÉÚÍÓÀÈÙÌÒÄËÜÏÖ ]+$/;
    return re.test(txt);
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}