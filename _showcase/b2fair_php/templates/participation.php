    <div class="row text-center">
      <a href="#content-first-block" class="d-inline-block mx-auto">
        <img src="assets/img/b2fair-fleche.svg" style="height: 70px;" />
      </a>
    </div>

    <div id="content-first-block" class="row row-max-width mx-auto pb-2 mb-2 text-center" style="padding-top: 100px;">
      <div class="col-lg-6 col-12 mt-5 side-img" style="min-height: 400px;" data-aos="fade-up" data-aos-duration="1500">
        <img class="floating floating-delay-1" src="assets/img/2106_B2FAIR_Register_Float2.png">
        <img class="floating floating-delay-2" src="assets/img/2106_B2FAIR_Register_Float3.png">
        <img class="floating" src="assets/img/2106_B2FAIR_Register_Float1.png">
      </div>
      <div class="col-lg-6 col-12 mt-5 pt-5 text-left">

        <h3 class="d-inline-block text-red p-1 mx-auto heading-30px font-bauer font-weight-bold" data-aos="fade-up">
          PARTICIPATION
        </h3>
        <h3 class="d-inline-block text-white bg-lightblue p-1 heading-26px" data-aos="flip-up">
          Deadline for registration 15th December 2021
        </h3>
        <p class="text-left pt-2 font-weight-bold" data-aos="fade-up">
          The participation fee amounts to EUR 125 per participant and includes the following services:
        </p>
        <p class="">
          <img class="m-1" src="assets/img/b2fair-bulletpoint.svg" style="width: 20px" />
          Individual b2b meetings<br />
          <img class="m-1" src="assets/img/b2fair-bulletpoint.svg" style="width: 20px" />
          Participation in workshops<br />
          <img class="m-1" src="assets/img/b2fair-bulletpoint.svg" style="width: 20px" />
          Visits to companies / Special Economic Zones including transfers<br />
          <img class="m-1" src="assets/img/b2fair-bulletpoint.svg" style="width: 20px" />
          Entrance ticket to the World Expo Dubai<br />
          <img class="m-1" src="assets/img/b2fair-bulletpoint.svg" style="width: 20px" />
          Participation in the networking receptions<br />
          <img class="m-1" src="assets/img/b2fair-bulletpoint.svg" style="width: 20px" />
          Lunch during the b2b meetings at the Expo
        </p>
      </div>
    </div>

    <div id="content-first-block" class="row row-max-width mx-auto pb-2 mb-2 text-center">
      <div class="col-lg-6 col-12 mt-5 pt-5 text-left">

        <h3 class="d-inline-block text-red p-1 mx-auto heading-30px font-bauer font-weight-bold" data-aos="fade-up">
          REGISTRATION PROCEDURES
        </h3>
        <div class="row mb-2">
          <div class="col" style="max-width: 75px">
            <span class="d-inline-block text-left font-weight-bold" style="width: 60px">Step 1:</span>
          </div>
          <div class="col">
            Register <a class="font-weight-bold"
              href="https://forms.office.com/Pages/ResponsePage.aspx?id=fF9TrbHdp0qojaE6cmk7ikZOWSlUPiFHgcNEkZ3IxSpUNEw5Vk9BN0tVS0FRV1JDODMwRVdUN1pBQSQlQCN0PWcu" target=_blank>here</a>.
            Create your company profile and define your company´s business cooperation
            requirements. Your profile will be shortly validated by the organiser / your local support office.
          </div>
        </div>
        <div class="row mb-2">
          <div class="col" style="max-width: 75px">
            <span class="d-inline-block text-left font-weight-bold" style="width: 60px">Step 2:</span>
          </div>
          <div class="col">
            Book meetings with the companies you desire to meet.
          </div>
        </div>
        <div class="row mb-2">
          <div class="col" style="max-width: 75px">
            <span class="d-inline-block text-left font-weight-bold" style="width: 60px">Step 3:</span>
          </div>
          <div class="col">
            Validate the meeting requests you have received and continue to monitor the event to book meetings with
            newly registered participants.
          </div>
        </div>
        <div class="row mb-2">
          <div class="col" style="max-width: 75px">
            <span class="d-inline-block text-left font-weight-bold" style="width: 60px">Step 4:</span>
          </div>
          <div class="col">
            A few days prior to the event, you will receive your provisional appointment schedule. Prepare for your
            meetings by studying the profiles of the companies that you are expected to meet.
          </div>
        </div>
        <div class="row mb-2">
          <div class="col" style="max-width: 75px">
            <span class="d-inline-block text-left font-weight-bold" style="width: 60px">Step 5:</span>
          </div>
          <div class="col">
            Your meeting schedule will be updated regularly during the event. Meet your potential business partners to
            discuss common areas of interest and to explore the possibility of cooperation and opportunities for the
            development of future bilateral projects
          </div>
        </div>
      </div>
      <div class="col-lg-6 mt-5 col-12 side-img" style="min-height: 400px; max-height: 600px;" data-aos="fade-up"
        data-aos-duration="1500">
        <img class="floating floating-delay-1" src="assets/img/2106_B2FAIR_Participation_Float2.png">
        <img class="floating floating-delay-2" src="assets/img/2106_B2FAIR_Participation_Float3.png">
        <img class="floating" src="assets/img/2106_B2FAIR_Participation_Float1.png">
      </div>
    </div>

    <div id="footer-image" class="row pt-5 mt-5">

      <div id="footer-gallery" class="w-100 d-flex flex-row justify-content-center align-bottom">
        <div class="px-4 py-5">
          <a class="text-decoration-none" href="https://www.b2fair.com/" target="_blank">
            <img src="assets/img/b2fair-logo-cc.svg" style="height: 50px;" class="mt-3" />
          </a>
        </div>
        <div class="px-4 py-5">
          <a class="text-decoration-none" href="https://www.luxembourgexpo2020dubai.lu/en/" target="_blank">
            <img src="assets/img/b2fair-logo-pavillon.svg" style="height: 65px;" />
          </a>
        </div>
        <div class="px-4 py-5 my-2">
          <a class="text-decoration-none" href="https://www.tradeandinvest.lu" target="_blank">
            <img src="assets/img/b2fair-logo-tradeinvest.svg" style="height: 25px;" class="mt-3" />
          </a>
        </div>
      </div>

      <div id="footer" class="w-100 m-0 p-0" style="font-size: 10px;">
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1"
          href="index.html">
          HOME
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="participation.html">
          PARTICIPATION, CONDITIONS & PROCEDURES
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="programme.html">
          PROGRAMME
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="partners.html">
          PARTNERS
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="participants.html">
          PARTICIPANTS
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="access.html" target=_blank>
          ACCESS
          THE PLATFORM</a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1"
          href="contact.html">
          CONTACT
        </a>
        <div class="py-1 pl-5 d-block d-lg-inline-block text-center float-right pr-2">
          &copy; Luxembourg Chamber of Commerce - All rights reserved 2021
        </div>
        <div class="clearfix" />
      </div>

    </div>