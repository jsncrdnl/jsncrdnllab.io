<header>
    <div id="overlayNav" class="overlay">
        <div class="overlay-content">
            <a href="?view=home">
                <sup class="text-muted">01</sup>
                HOME
            </a>
            <a href="?view=participation">
                <sup class="text-muted">02</sup>
                PARTICIPATION, CONDITIONS & PROCEDURES
            </a>
            <a href="?view=programme">
                <sup class="text-muted">03</sup>
                PROGRAMME
            </a>
            <a href="?view=partners">
                <sup class="text-muted">04</sup>
                PARTNERS
            </a>
            <a href="?view=participants">
                <sup class="text-muted">05</sup>
                PARTICIPANTS
            </a>
            <a href="?view=access" target=_blank>
                <sup class="text-muted">06</sup>
                ACCESS THE PLATFORM</a>
            <a href="?view=contact">
                <sup class="text-muted">07</sup>
                CONTACT
            </a>
        </div>
    </div>
</header>