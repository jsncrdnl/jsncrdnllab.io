<div class="row text-red ml-5 mt-5 pt-5 pl-5 pb-3">
    <h1 class="heading-60px font-bauer font-weight-bold">
        INTERNATIONAL BUSINESS<br />
        MEETINGS by <span class="text-blue font-serif font-weight-bold">b2<i class="font-weight-bold">fair</i></span>
    </h1>
</div>
<div class="row text-black ml-5 pl-5 pb-3 heading-30px">
    CONNECTING BUSINESSES,<br />
    CREATING GROWTH @ THE WORLD EXPO 2020
</div>
<div class="row ml-5 pl-5">
    <h3 class="d-inline-block text-white bg-lightblue p-1 heading-26px">
        23rd - 25th JANUARY 2022 // DUBAI<br />
    </h3>
</div>