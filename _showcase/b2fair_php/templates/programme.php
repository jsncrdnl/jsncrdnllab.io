    <div class="row text-center">
      <a href="#content-first-block" class="d-inline-block mx-auto">
        <img src="assets/img/b2fair-fleche.svg" style="height: 70px;" />
      </a>
    </div>

    <div id="content-first-block" class="row row-max-width mx-auto pb-2 mb-2 text-center" style="padding-top: 100px;">

      <iframe src="https://app.swapcard.com/widget/event/international-business-meetings-by-b2fair/plannings/RXZlbnRWaWV3XzIwNzk1OQ==?showActions=true" style="border: none; margin: 0px; width: 100%; display: block; height: calc(100vh - 100px);"></iframe>

    </div>

    <div id="footer-image" class="row pt-5 mt-5">

      <div id="footer-gallery" class="w-100 d-flex flex-row justify-content-center align-bottom">
        <div class="px-4 py-5">
          <a class="text-decoration-none" href="https://www.b2fair.com/" target="_blank">
            <img src="assets/img/b2fair-logo-cc.svg" style="height: 50px;" class="mt-3" />
          </a>
        </div>
        <div class="px-4 py-5">
          <a class="text-decoration-none" href="https://www.luxembourgexpo2020dubai.lu/en/" target="_blank">
            <img src="assets/img/b2fair-logo-pavillon.svg" style="height: 65px;" />
          </a>
        </div>
        <div class="px-4 py-5 my-2">
          <a class="text-decoration-none" href="https://www.tradeandinvest.lu" target="_blank">
            <img src="assets/img/b2fair-logo-tradeinvest.svg" style="height: 25px;" class="mt-3" />
          </a>
        </div>
      </div>

      <div id="footer" class="w-100 m-0 p-0" style="font-size: 10px;">
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="index.html">
          HOME
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="participation.html">
          PARTICIPATION, CONDITIONS & PROCEDURES
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="programme.html">
          PROGRAMME
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="partners.html">
          PARTNERS
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="participants.html">
          PARTICIPANTS
        </a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="access.html" target=_blank>
          ACCESS
          THE PLATFORM</a>
        <a class="text-decoration-none d-block d-md-inline-block text-center text-black sm-float-left p-1" href="contact.html">
          CONTACT
        </a>
        <div class="py-1 pl-5 d-block d-lg-inline-block text-center float-right pr-2">
          &copy; Luxembourg Chamber of Commerce - All rights reserved 2021
        </div>
        <div class="clearfix" />
      </div>

    </div>