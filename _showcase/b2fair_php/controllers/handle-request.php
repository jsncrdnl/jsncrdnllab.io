<?php


// PARSE REQUESTED PAGE INFO
$view   = isset($_GET['view'])   ? $_GET['view'] : 'home';
// $data   = isset($_GET['data']) ? $_GET['data'] : NULL;
// $id     = isset($_GET['id'])   ? intval($_GET['id']) : NULL;

if (!file_exists("templates/$view.php")) {
    die('404 :: Cette page n\'existe pas !');
}
