<?php
session_start();

// require 'db_manager.php';
// require 'functions.php';

// MAKE SURE DB EXISTS
// $db = openDb();
// createDbUsersIfNotExisting($db);
// createDbIfNotExisting($db);
// loadSeedsIfEmpty($db);
// closeDb($db);

require 'controllers/handle-request.php';
require 'controllers/handle-async.php';
require 'controllers/handle-forms.php';
