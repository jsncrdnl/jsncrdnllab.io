<?php

class Spdo{

	public $sql = false;
	public $obj = false;
	public $prm = false;
	public $state = false;

	public function __construct($config){
		$config = (object)$config;
		try{
			$this->pdo = new PDO('mysql:dbname='.$config->db.';host='.$config->host,$config->user ,$config->password);
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->pdo->exec("SET lc_time_names = 'fr_FR'");
		}catch(Exception $e){
			exit($e);
		}
	}

	public function make($array){
		$sql = $obj = ""; $prm = array(); $error = false;
		extract($array);

		$this->sql = $sql;
		$this->obj = $obj;
		$this->prm = $prm;

		$this->sql = $this->pdo->prepare($this->sql);

		if( $error ){
			$this->sqld();
		}

		$this->state = $this->sql->execute($prm);

		return $this;
	}

	public function listing($key=""){
		$class = $this->obj;

		$array = array();
		while($element = $this->sql->fetch(PDO::FETCH_ASSOC)){
			if(!empty($class)){
				$var = new $class($element);
			}else{
				$var = $element;
			}

			if( !empty($key) && isset($element[$key]) ){
				$array[$element[$key]] = $var;
			}else{
				$array[] = $var;
			}
		}

		return $array;
	}

	public function first(){
		$class = $this->obj;
		$element = $this->sql->fetch(PDO::FETCH_ASSOC);

		if( $this->obj && $element ){
			$element = new $class($element);
		}

		if(!$element) return false;
		else{
			return $element;
		}
	}
	
	public function insert($data){}

}


