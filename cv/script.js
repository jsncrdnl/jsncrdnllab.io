// Experience
var text_xp = 
"My XP\n\n"+
"Web technologies\n"+
"HTML/ CSS/ JS/ PHP/ Symfony/ Laravel/ AJAX/ BOOTSTRAP/ NodeJS/ AngularJS/ VueJS/ CMS/ Django\n\n"+
"Desktop development\n" + 
"C/ C++/ C#/ Java/ Python/ R/ Shell/ Windev\n\n"+
"Mobile/Cross-platform\n"+
"Android Native/ Apache Cordova/ Ionic Framework/ Phonegap/ Coco3d/ Unity3D/ Windev Mobile\n\n"+
"Database & flat-file storage\n"+
"MySQL/ MS SQL/ pgSQL/ XML/ JSON/ YML\n\n" +
"Platform & Cloud\n"+ 
"UNIX/ OVH/ Redshift/ Amazon AWS/ Heroku\n\n" +
"Versionning\n"+
"SVN/ Git/ Gitlab/ CI-CD";

// Formation
var text_form = 
"My quest\n\n" +
// "2001 > 2007 | Collège Sainte-Marie\n\n" +
// "2007 > 2008 | ISEP\n" +
// "Studying foreign languages (English & Dutch)\n\n" +
// "2008 > 2011 | ISIMs\n" +
// "IT & system, network orientation\n\n" + 
"2011 > 2018 | Optimal Computing\n" + 
"Full-stack lead developer\n\n" + 
"2016 > 2018 | TWOCAN\n" + 
"Co-founder of this web design startup as a complementary activity\n\n" + 
"2018 > 2020 | UP TO BE\n" + 
"Founder of this web design & electronic (RPI/Arduino) startup as a complementary activity" +
"Co-founder of this web design startup as a complementary activity\n\n" + 
"2018 > 2021 | Meaweb SPRL\n" + 
"Lead backend developper" + 
"2021 > Now | SPF BOSA\n" + 
"Full-Stack PHP developper";

// Formation
var text_folio = 
"Credits\n\n" + 
"Follow this link to discover my work\n" + 
"https://jsncrdnl.gitlab.io/portfolio";

// Contact
var text_contact = 
"Author\n\n" + 
"This Curriculum Vitae website has been developped by Jason Cardinal.\n\n" + 
"Wanna get it touch ?\n" + 
"Drop me a mail at jsncrdnl@gmail.com\n"+
"Or visit my website: https://jsncrdnl.gitlab.io\n";

var consoleStart = ''; 

function linkify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    // replacedText = replacedText.replace(/jsncrdnl@gmail.com/g, '<a href="mailto:jsncrdnl@gmail.com">jsncrdnl@gmail.com</a>');
    
    return replacedText;
}

var processing_link;
var showtxtTimer;
var showText = function (message, index, interval)
{   
    if(showtxtTimer!=null) clearTimeout(showtxtTimer);
    
    if(index==0) $("#msgBlock").html("");
    if (index < message.length) {
        keyaudio.play();
        var tmp = message[index++];
        if(tmp=='\n') 	$("#msgBlock").append( "<br/>" + consoleStart );
        else 			$("#msgBlock").append( tmp );
        showtxtTimer = setTimeout(function () { showText(message, index, interval); }, interval);
  }
  else {
    $("#msgBlock").append( "<br/>" + consoleStart ); 
    $("#msgBlock").html( linkify( $("#msgBlock").html() ) );
  }
};

var go_up = function(){

    navaudio.play();
    
    $cur = $(".console.selected"); 
    if($cur.prev().length==1)   $cur.prev().addClass("selected"); 
    else                        $(".console:last").addClass("selected"); 
    $cur.removeClass("selected");

};

var go_down = function(){

    navaudio.play();
    
    $cur = $(".console.selected"); 
    if($cur.next().length==1)   $cur.next().addClass("selected"); 
    else                        $(".console:first").addClass("selected"); 
    $cur.removeClass("selected");

};

var select_action = function()
{    
    okaudio.play();
         
    var target = $('.console.selected').attr("data-target");
    if(showtxtTimer!=null) clearTimeout(showtxtTimer);     
    $(".infopanel").toggleClass('visiblepanel');

    if ( $(".infopanel").hasClass('visiblepanel') )
    { 
        var text = (target=="xp") ? text_xp : (target=="form") ? text_form : (target=="folio") ? text_folio : text_contact;
        showText(text, 0, 50);
    }
};

var navaudio = new Audio('Final_Fantasy_VII_-_Cursor_Move_crappy.mp3');
var okaudio = new Audio('Final_Fantasy_VII__Accept_crappy.mp3');
var bgaudio = new Audio('Machine_Head-Imperium_8Bit_crappy.mp3');
var keyaudio = new Audio('keyboard_effect.wav');

var enabledbgaudio = function(){
    bgaudio.play();
};

var xDown = null;                                                        
var yDown = null;                                                        

function handleTouchStart(evt) {                                         
    xDown = evt.touches[0].clientX;                                      
    yDown = evt.touches[0].clientY;                                      
};                                                

function handleTouchMove(evt) {
    if ( ! xDown || ! yDown ) select_action();
    else
    {
        var xUp = evt.touches[0].clientX;                                    
        var yUp = evt.touches[0].clientY;
        var xDiff = xDown - xUp;
        var yDiff = yDown - yUp;

        if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {
            if ( xDiff > 0 )    go_up();
            else                go_down();    
        } else {
            if ( yDiff > 0 )    go_down();
            else                go_up();                                                                          
        }
        xDown = null;
    }
}

$(document).ready(function() 
{ 
    document.addEventListener('touchstart', handleTouchStart, false);        
    document.addEventListener('touchmove', handleTouchMove, false);

    $(document).click(enabledbgaudio); 
    
    $(".infopanel").click(select_action);
    
    $(".console").hover(function(){
        navaudio.play();
        enabledbgaudio();
        $(".console").removeClass("selected");
        $(this).addClass("selected")
    }, function(){    
    });
    $(".console").click(select_action);
    
    $(document).keydown(function(e) {
        enabledbgaudio();
        switch(e.which) {
            case 37: /* LEFT */     go_up();        break;
            case 38: /* UP */       go_up();        break;
            case 39: /* RIGHT */    go_down();      break;
            case 40: /* DOWN */     go_down();      break;            
            case 13: /* ENTER */    select_action();break;
            case 32: /* SPACE */    select_action();break;
            default: return;
        }
        e.preventDefault();
    });
    
    
    
}); 